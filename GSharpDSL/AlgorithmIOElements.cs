﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GSharpDSL
{
    /// <summary>
    /// The class that handles the DSL to assemble an Algorithm.
    /// </summary>
    public class AlgorithmInterface
    {
        private static readonly int M_NODEDATA_CHECK = 0;
        private static readonly int M_EDGEDATA_CHECK = 1;
        private static readonly int M_GRAPHDATA_CHECK = 2;
        private static readonly int M_NAME_CHECK = 3;
        private static readonly int M_NAMESPACE_CHECK = 4;
        private static readonly int M_INPUTS_CHECK = 5;
        private static readonly int M_OUTPUTS_CHECK = 6;
        private static readonly int M_END_CHECK = 7;

        private int[] m_callCounter = new int[8];

        internal List<string> M_NamespaceImports { get; } = new List<string>();
        internal NodeInfo M_NodeData { get; private set; } = null;
        internal EdgeInfo M_EdgeData { get; private set; } = null;
        internal GraphInfo M_GraphData { get; private set; } = null;
        internal string M_Name { get; private set; } = "";
        internal string M_Namespace { get; private set; } = "";
        internal List<Input> M_Inputs { get; } = new List<Input>();
        internal List<Output> M_Outputs { get; } = new List<Output>();

        /// <summary>
        /// The algorithm's name. (Required)
        /// </summary>
        /// <param name="name">The desired name.</param>
        public AlgorithmInterface InterfaceName(String name)
        {
            m_callCounter[M_NAME_CHECK]++;
            M_Name = name;
            return this;
        }

        /// <summary>
        /// (Required) The Type of information that will be stored in the graph's nodes. 
        /// </summary>
        /// <param name="nodeInfoType">The desired Type.</param>
        public NodeInfo NodeInfo(Type nodeInfoType)
        {
            m_callCounter[M_NODEDATA_CHECK]++;
            M_NodeData = new NodeInfo(this, nodeInfoType);
            return M_NodeData;
        }

        /// <summary>
        /// (Required) The Type of information that will be stored in the graph's edges. 
        /// </summary>
        /// <param name="edgeInfoType">The desired Type.</param>
        public EdgeInfo EdgeInfo(Type edgeInfoType)
        {
            m_callCounter[M_EDGEDATA_CHECK]++;
            M_EdgeData = new EdgeInfo(this, edgeInfoType);
            return M_EdgeData;
        }

        /// <summary>
        /// (Required) The Type of information that will be stored in the whole graph. 
        /// </summary>
        /// <param name="graphInfoType">The desired Type.</param>
        public GraphInfo GraphInfo(Type graphInfoType)
        {
            m_callCounter[M_GRAPHDATA_CHECK]++;
            M_GraphData = new GraphInfo(this, graphInfoType);
            return M_GraphData;
        }

        /// <summary>
        /// (Optional) Adds a configurable input to this algorithm. 
        /// </summary>
        public Input AddInput()
        {
            m_callCounter[M_INPUTS_CHECK]++;
            Input input = new Input(this);
            M_Inputs.Add(input);
            return input;
        }

        /// <summary>
        /// (Optional) Adds a configurable output to this algorithm. 
        /// </summary>
        public Output AddOutput()
        {
            m_callCounter[M_OUTPUTS_CHECK]++;
            Output output = new Output(this);
            M_Outputs.Add(output);
            return output;
        }

        /// <summary>
        /// (Optional) Adds an import to this algorithm.
        /// </summary>
        /// <param name="namespaceImport">The string which contains the import's full name. Is case-sensitive (ex. System.Collections).</param>
        public AlgorithmInterface AddNamespaceImport(string namespaceImport)
        {
            M_NamespaceImports.Add(namespaceImport);
            return this;
        }

        /// <summary>
        /// (Optional) The desired namespace for the algorithm. 
        /// </summary>
        /// <param name="codeNamespace">The string which contains the namespace name. Is case-sensitive (GSharpDSL is the default namespace).</param>
        public AlgorithmInterface CodeNamespace(string codeNamespace)
        {
            m_callCounter[M_NAMESPACE_CHECK]++;
            M_Namespace = codeNamespace;
            return this;
        }

        /// <summary>
        /// (Required) Declares the end of the algorithm's interface and returns the assembled object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;

            if (!CheckDslValidity())
            {
                Console.WriteLine("--------^ For interface : " + M_Name + " ------------------");
                throw new Exception("INVALID DSL!");
            }

            if (M_Namespace.Equals(""))
            {
                // Get current namespace
                M_Namespace = GetType().Namespace;
            }

            //--------Data output------------
            StreamWriter sw = new StreamWriter("AlgorithmInterfaceDataOutput.txt");
            WriteDataToFile(sw);
            sw.Close();

            //--------Generated code---------
            CodeGenerator cg = new CodeGenerator(this);
            StreamWriter sw1 = new StreamWriter("../../" + M_Name + "_Core.cs");

            if (!File.Exists("../../" + M_Name + ".cs"))
            {
                StreamWriter sw2 = new StreamWriter("../../" + M_Name + ".cs");
                cg.GenerateAndEmitInterface(sw1, sw2);
                sw2.Close();
            }
            else
            {
                cg.GenerateAndEmitInterface(sw1);
            }

            sw1.Close();
            return this;
        }

        private bool CheckDslValidity()
        {
            bool check = true;

            if (m_callCounter[M_NAME_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (interface) : InterfaceName() Missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_NAMESPACE_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (interface) : CodeNamespace() Called more than once");
                check = false;
            }

            if (m_callCounter[M_NODEDATA_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (interface) : NodeInfo() Missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_EDGEDATA_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (interface) : EdgeInfo() Missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_GRAPHDATA_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (interface) : GraphInfo() Missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (interface) : end() Missing or called more than once");
                check = false;
            }

            if (M_NodeData!= null && !M_NodeData.ValidityCheck())
                check = false;
            if (M_EdgeData != null && !M_EdgeData.ValidityCheck())
                check = false;
            if (M_GraphData != null && !M_GraphData.ValidityCheck())
                check = false;

            if (m_callCounter[M_INPUTS_CHECK] > 0)
            {
                foreach (var input in M_Inputs)
                {
                    if (!input.CheckValidity())
                        check = false;
                }
            }

            if (m_callCounter[M_OUTPUTS_CHECK] > 0)
            {
                foreach (var output in M_Outputs)
                {
                    if (!output.CheckValidity())
                        check = false;
                }
            }

            return check;
        }

        private void WriteDataToFile(StreamWriter outputFile)
        {
            int hash = GetHashCode();
            outputFile.WriteLine("");
            outputFile.WriteLine($"------ AlgorithmInterface @{hash} ------");
            outputFile.WriteLine("");
            WriteNodeDataToFile(outputFile);
            WriteEdgeDataToFile(outputFile);
            WriteGraphDataToFile(outputFile);
            outputFile.WriteLine("______________________________");
            outputFile.WriteLine("");
            outputFile.WriteLine($"------ END @{hash} ------");
        }

        private void WriteNodeDataToFile(StreamWriter outputFile)
        {
            outputFile.WriteLine("______________________________");
            outputFile.WriteLine("| ++++ Node Data : ++++ |");
            outputFile.WriteLine(M_NodeData.ToString());
            outputFile.WriteLine("| ++++ End Node Data ++ |");
        }
        private void WriteEdgeDataToFile(StreamWriter outputFile)
        {
            outputFile.WriteLine("______________________________");
            outputFile.WriteLine("| ++++ Edge Data : ++++ |");
            outputFile.WriteLine(M_EdgeData.ToString());
            outputFile.WriteLine("| ++++ End Edge Data ++ |");
        }
        private void WriteGraphDataToFile(StreamWriter outputFile)
        {
            outputFile.WriteLine("______________________________");
            outputFile.WriteLine("| ++++ Graph Data : ++++ |");
            outputFile.WriteLine(M_GraphData.ToString());
            outputFile.WriteLine("| ++++ End Graph Data ++ |");
        }

    }

    public class NodeInfo
    {
        private static readonly int M_MEMBERS_CHECK = 0;
        private static readonly int M_END_CHECK = 1;

        private AlgorithmInterface m_father;
        private int[] m_callCounter = new int[2];

        internal Members M_NodeInfoMembers { get; private set; }
        internal Type M_NodeInfoType { get; }
        internal string M_GetterName { get; private set; }
        internal string M_SetterName { get; private set; }

        public NodeInfo(AlgorithmInterface father, Type nodeInfoType)
        {
            M_NodeInfoType = nodeInfoType;
            m_father = father;
        }

        /// <summary>
        /// (Optional) Add potential encapsulated class members inside this information Type. 
        /// </summary>
        public Members Members()
        {
            m_callCounter[M_MEMBERS_CHECK]++;
            Members members = new Members(m_father);
            M_NodeInfoMembers = members;
            return members;
        }

        /// <summary>
        /// (Optional) Creates a getter for this information Type. 
        /// </summary>
        public NodeInfo CreateGetter(string name)
        {
            M_GetterName = name;
            return this;
        }

        /// <summary>
        /// (Optional) Creates a setter for this information Type. 
        /// </summary>
        public NodeInfo CreateSetter(String name)
        {
            M_SetterName = name;
            return this;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool ValidityCheck()
        {
            bool check = true;

            if (m_callCounter[M_MEMBERS_CHECK] == 1)
            {
                if (!M_NodeInfoMembers.ValidityCheck())
                    check = false;
            }
            else if (m_callCounter[M_MEMBERS_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (NodeInfo) : Members() called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (NodeInfo) : end() called more than once");
                check = false;
            }

            return check;
        }

        public override string ToString()
        {
            String str = "  -+ Node Info Type : " + M_NodeInfoType.CSharpName();

            if (M_NodeInfoMembers == null || !M_NodeInfoMembers.M_Members.Any())
                return str;

            foreach (Member m in M_NodeInfoMembers.M_Members)
            {
                str += "\n  -+ Member (" + m.M_Name + ") :  Type (" + m.M_MemberInfoType.CSharpName() + "), Path " + "(" + m.M_MemberPath + ")";
            }
            return str;
        }
    }

    public class EdgeInfo
    {
        private static readonly int M_MEMBERS_CHECK = 0;
        private static readonly int M_END_CHECK = 1;

        private AlgorithmInterface m_father;
        private int[] m_callCounter = new int[2];

        internal Members M_EdgeInfoMembers { get; private set; }
        internal Type M_EdgeInfoType { get; }
        internal string M_GetterName { get; private set; }
        internal string M_SetterName { get; private set; }

        public EdgeInfo(AlgorithmInterface father, Type edgeInfoType)
        {
            M_EdgeInfoType = edgeInfoType;
            m_father = father;
        }

        /// <summary>
        /// (Optional) Add potential encapsulated class members inside this information Type. 
        /// </summary>
        public Members Members()
        {
            m_callCounter[M_MEMBERS_CHECK]++;
            Members members = new Members(m_father);
            M_EdgeInfoMembers = members;
            return members;
        }

        /// <summary>
        /// (Optional) Creates a getter for this information Type. 
        /// </summary>
        public EdgeInfo CreateGetter(string name)
        {
            M_GetterName = name;
            return this;
        }

        /// <summary>
        /// (Optional) Creates a setter for this information Type. 
        /// </summary>
        public EdgeInfo CreateSetter(String name)
        {
            M_SetterName = name;
            return this;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool ValidityCheck()
        {
            bool check = true;

            if (m_callCounter[M_MEMBERS_CHECK] == 1)
            {
                if (!M_EdgeInfoMembers.ValidityCheck())
                    check = false;
            }
            else if (m_callCounter[M_MEMBERS_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (EdgeInfo) : Members() called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (EdgeInfo) : end() called more than once");
                check = false;
            }

            return check;
        }

        public override string ToString()
        {
            String str = "  -+ Edge Info Type : " + M_EdgeInfoType.CSharpName();

            if (M_EdgeInfoMembers == null || !M_EdgeInfoMembers.M_Members.Any())
                return str;

            foreach (Member m in M_EdgeInfoMembers.M_Members)
            {
                str += "\n  -+ Member (" + m.M_Name + ") :  Type (" + m.M_MemberInfoType.CSharpName() + "), Path " + "(" + m.M_MemberPath + ")";
            }
            return str;
        }
    }

    public class GraphInfo
    {
        private static readonly int M_MEMBERS_CHECK = 0;
        private static readonly int M_END_CHECK = 1;

        private AlgorithmInterface m_father;
        private int[] m_callCounter = new int[2];

        internal Members M_GraphInfoMembers { get; private set; }
        internal Type M_GraphInfoType { get; }
        internal string M_GetterName { get; private set; }
        internal string M_SetterName { get; private set; }

        public GraphInfo(AlgorithmInterface father, Type graphInfoType)
        {
            M_GraphInfoType = graphInfoType;
            m_father = father;
        }

        /// <summary>
        /// (Optional) Add potential encapsulated class members inside this information Type. 
        /// </summary>
        public Members Members()
        {
            m_callCounter[M_MEMBERS_CHECK]++;
            Members members = new Members(m_father);
            M_GraphInfoMembers = members;
            return members;
        }

        /// <summary>
        /// (Optional) Creates a getter for this information Type. 
        /// </summary>
        public GraphInfo CreateGetter(string name)
        {
            M_GetterName = name;
            return this;
        }

        public GraphInfo CreateSetter(String name)
        {
            M_SetterName = name;
            return this;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool ValidityCheck()
        {
            bool check = true;

            if (m_callCounter[M_MEMBERS_CHECK] == 1)
            {
                if (!M_GraphInfoMembers.ValidityCheck())
                    check = false;
            }
            else if (m_callCounter[M_MEMBERS_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (GraphInfo) : Members() called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (GraphInfo) : end() called more than once");
                check = false;
            }

            return check;
        }

        public override string ToString()
        {
            String str = "  -+ Graph Info Type : " + M_GraphInfoType.CSharpName();

            if (M_GraphInfoMembers == null || !M_GraphInfoMembers.M_Members.Any())
                return str;

            foreach (Member m in M_GraphInfoMembers.M_Members)
            {
                str += "\n  -+ Member (" + m.M_Name + ") :  Type (" + m.M_MemberInfoType.CSharpName() + "), Path " + "(" + m.M_MemberPath + ")";
            }
            return str;
        }
    }

    public class Members
    {
        private static readonly int M_MEMBER_CHECK = 0;
        private static readonly int M_END_CHECK = 1;

        private int[] m_callCounter = new int[2];
        private AlgorithmInterface m_father;

        internal List<Member> M_Members { get; } = new List<Member>();
        public Members(AlgorithmInterface father)
        {
            m_father = father;
        }

        /// <summary>
        /// (Required) Add an encapsulated class member. 
        /// </summary>
        /// <param name="memberInfoType">The member Type.</param>
        public Member Member(Type memberInfoType)
        {
            m_callCounter[M_MEMBER_CHECK]++;
            Member member = new Member(this, memberInfoType);
            M_Members.Add(member);
            return member;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool ValidityCheck()
        {
            bool check = true;

            if (m_callCounter[M_MEMBER_CHECK] > 0)
            {
                foreach (var member in M_Members)
                {
                    bool b = member.CheckValidity();
                    if (!b)
                        check = false;
                }
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (Members) : end() called more than once");
                check = false;
            }

            return check;
        }
    }

    public class Member
    {
        private static readonly int M_NAME_CHECK = 0;
        private static readonly int M_END_CHECK = 1;

        private int[] m_callCounter = new int[2];
        private Members m_father;

        internal string M_Name { get; private set; }
        internal Type M_MemberInfoType { get; }
        internal string M_MemberPath { get; private set; } = "";

        public Member(Members father, Type memberInfoType)
        {
            M_MemberInfoType = memberInfoType;
            m_father = father;
        }

        /// <summary>
        /// (Required) Add the member's name. 
        /// </summary>
        /// <param name="name">Name</param>
        public Member MemberName(String name)
        {
            m_callCounter[M_NAME_CHECK]++;
            M_Name = name;
            if (M_MemberPath.Equals(""))
                M_MemberPath = M_Name;
            return this;
        }

        /// <summary>
        /// (Optional) Add the member's full encapsulated path. (Ex. ClassName1.ClassName2.MemberX) 
        /// </summary>
        /// <param name="path">The path. Is case-sensitive.</param>
        public Member MemberPath(String path)
        {
            M_MemberPath = path;
            return this;
        }


        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public Members end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool CheckValidity()
        {
            bool check = true;

            if (m_callCounter[M_NAME_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (Member) : MemberName() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (Member) : end() called more than once");
                check = false;
            }

            return check;
        }
    }

    public class Input
    {
        private static readonly int M_NAME_CHECK = 0;
        private static readonly int M_TYPE_CHECK = 1;
        private static readonly int M_CONNECTION_CHECK = 2;
        private static readonly int M_END_CHECK = 3;

        private int[] m_callCounter = new int[4];
        private AlgorithmInterface m_father;

        public string M_InputName { get; private set; }
        public Type M_InputType { get; private set; }
        public string M_ConnectionFromKey { get; private set; }

        public Input(AlgorithmInterface father)
        {
            m_father = father;
        }

        /// <summary>
        /// (Required) Set a name for this input. 
        /// </summary>
        /// <param name="name">Input name</param>
        public Input Name(string name)
        {
            m_callCounter[M_NAME_CHECK]++;
            M_InputName = name;
            return this;
        }

        /// <summary>
        /// (Required) Set the type for this input. 
        /// </summary>
        /// <param name="type">The input Type</param>
        public Input Type(Type type)
        {
            m_callCounter[M_TYPE_CHECK]++;
            M_InputType = type;
            return this;
        }

        /// <summary>
        /// (Required) Set the incoming connection from Bank.
        /// </summary>
        /// <param name="fromBankKey">The key which will be used to get the incoming connection's data.</param>
        public Input ConnectionFrom(string fromBankKey)
        {
            m_callCounter[M_CONNECTION_CHECK]++;
            M_ConnectionFromKey = fromBankKey;
            return this;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;
            return m_father;
        }

        internal bool CheckValidity()
        {
            bool check = true;

            if (m_callCounter[M_NAME_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (AddInput) : Name() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_TYPE_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (AddInput) : Type() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_CONNECTION_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (AddInput) : ConnectionFrom() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (AddInput) : end() called more than once");
                check = false;
            }

            return check;
        }
    }

    public class Output
    {
        private static readonly int M_NAME_CHECK = 0;
        private static readonly int M_CONNECTION_CHECK = 1;
        private static readonly int M_END_CHECK = 2;

        private int[] m_callCounter = new int[3];
        private AlgorithmInterface m_father;

        public string M_OutputName { get; private set; }
        public string M_ConnectionToKey { get; private set; }

        public Output(AlgorithmInterface father)
        {
            m_father = father;
        }

        /// <summary>
        /// (Required) Set a name for this output. 
        /// </summary>
        /// <param name="name">Output name</param>
        public Output Name(string name)
        {
            m_callCounter[M_NAME_CHECK]++;
            M_OutputName = name;
            return this;
        }

        /// <summary>
        /// (Required) Set the outgoing connection to Bank.
        /// </summary>
        /// <param name="toBankKey">The key which will be used to store the outgoing connection's data.</param>
        public Output ConnectTo(string toBankKey)
        {
            m_callCounter[M_CONNECTION_CHECK]++;
            M_ConnectionToKey = toBankKey;
            return this;
        }

        /// <summary>
        /// (Required) Return the object. 
        /// </summary>
        public AlgorithmInterface end()
        {
            m_callCounter[M_END_CHECK]++;

            if(M_ConnectionToKey != null)
                IOBank.AddItemToBank(M_ConnectionToKey, null);

            return m_father;
        }

        internal bool CheckValidity()
        {
            bool check = true;

            if (m_callCounter[M_NAME_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (AddOutput) : Name() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_CONNECTION_CHECK] != 1)
            {
                Console.WriteLine("Invalid DSL (AddOutput) : ConnectTo() missing or called more than once");
                check = false;
            }

            if (m_callCounter[M_END_CHECK] > 1)
            {
                Console.WriteLine("Invalid DSL (AddOutput) : end() called more than once");
                check = false;
            }

            return check;
        }
    }
}

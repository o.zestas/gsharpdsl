using GraphLibrary;
using System;
using System.Collections.Generic;
using GraphLibrary.Algorithms;

namespace GSharpDSL {

    public partial class AlgTwo {     

        public override void Init() {        
            /*TODO: Write Init function!*/        
            InitializeInputs();        
        }        

        public override int Run() {        
            Init();        
            /*TODO: Write Run function!        
            * Don't forget to store your outputs to IOBank!*/        
            StoreOutput(MYOUTPUT_KEY,null);       
        
            return 0;        
        }        

    }

}

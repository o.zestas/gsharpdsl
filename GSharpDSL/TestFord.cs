using System;
using System.Collections;
using GraphLibrary;
using System.Collections.Generic;
using GraphLibrary.Algorithms;

namespace GSharpDSL {

    public partial class TestFord {
        private CGraphQueryInfo<object, int, object> m_inputGraphWeightInfo;
        private CGraph mGraph;
        private CGraphNode m_source;
        public const int m_PATHINFO = 0;
        public const int m_SHORTESTPATHSINFO = 1;
        private Dictionary<CGraphNode, Dictionary<CGraphNode, Path>> m_shortestPaths =
            new Dictionary<CGraphNode, Dictionary<CGraphNode, Path>>();

        public TestFord(CGraph graph, CGraphNode source, object graphKEY) : this(graph, graphKEY)
        {
            mGraph = graph;
            m_source = source;
            this[m_PATHINFO] = _outputTestFordQueryInfo = new TestFordQueryInfo(graph, this);
            m_inputGraphWeightInfo = new CGraphQueryInfo<object, int, object>(graph, graphKEY);
            _outputTestFordQueryInfo.CreateInfo(m_shortestPaths);
        }

        public void FindAllPairsShortestPaths()
        {
            CIt_GraphNodes itn = new CIt_GraphNodes(mGraph);

            for (itn.Begin(); !itn.End(); itn.Next())
            {
                m_source = itn.M_CurrentItem;
                Run();
            }
        }

        public override void Init() {
            CIt_GraphNodes itn = new CIt_GraphNodes(mGraph);

            m_shortestPaths[m_source] = new Dictionary<CGraphNode, Path>();

            for (itn.Begin(); !itn.End(); itn.Next())
            {
                if (itn.M_CurrentItem != m_source)
                {
                    _outputTestFordQueryInfo.CreateInfo(itn.M_CurrentItem,
                        new NodePathInfo() { MDistance = null, MPredecessor = null });
                }
                else
                {
                    _outputTestFordQueryInfo.CreateInfo(itn.M_CurrentItem,
                        new NodePathInfo() { MDistance = 0, MPredecessor = null });
                }
            }
        }        

        public override int Run() {
            Init();

            CIt_GraphEdges ite = new CIt_GraphEdges(mGraph);
            for (int i = 0; i < mGraph.M_NumberOfNodes - 1; i++)
            {
#if DEBUG
                Console.WriteLine("Iteration : {0}", i);
#endif
                for (ite.Begin(); !ite.End(); ite.Next())
                {
                    Relax(ite.M_CurrentItem.M_Source, ite.M_CurrentItem.M_Target);
                }
            }

            for (ite.Begin(); !ite.End(); ite.Next())
            {
                if (MDistance(ite.M_CurrentItem.M_Target) > MDistance(ite.M_CurrentItem.M_Source) +
                    Weight(ite.M_CurrentItem.M_Source, ite.M_CurrentItem.M_Target))
                {
                    throw new Exception("The graph contains negative cycles");
                }
            }
            GeneratePaths();

#if DEBUG
            Debug();
#endif

            return 0;
        }

        private void GeneratePaths()
        {
            CIt_GraphNodes it = new CIt_GraphNodes(mGraph);
            CGraphNode m_node;

            for (it.Begin(); !it.End(); it.Next())
            {
                m_shortestPaths[m_source][it.M_CurrentItem] = new Path();
                m_node = it.M_CurrentItem;
                while (m_node != null)
                {
                    m_shortestPaths[m_source][it.M_CurrentItem].Insert(m_node, 0);
                    m_node = MPredecessor(m_node);
                }
            }
        }

        protected void Relax(CGraphNode source, CGraphNode target)
        {
            int currentDistance;

            if (MDistance(source) != null)
            {
                currentDistance = (int)MDistance(source) + Weight(source, target);

                if (MDistance(target) != null)
                {
                    if (MDistance(target) > currentDistance)
                    {
                        SetMDistance(target, currentDistance);
                        SetMPredecessor(target, source);
                    }
                }
                else
                {
                    SetMDistance(target, currentDistance);
                    SetMPredecessor(target, source);
                }
            }
            else
            {
#if DEBUG
                currentDistance = Weight(source, target);
#endif
            }

#if DEBUG
            Console.WriteLine("Relax: {0} -->  {1}  CurrentDistance={2}, Distance={3}, Weight={4}",
                source.M_Label, target.M_Label, currentDistance, MDistance(target), Weight(source, target));
#endif
        }

        protected int Weight(CGraphNode source, CGraphNode target)
        {
            return (int)(m_inputGraphWeightInfo.Info(source, target));
        }

        private void Debug()
        {
            CIt_GraphNodes it = new CIt_GraphNodes(mGraph);

            for (it.Begin(); !it.End(); it.Next())
            {
                Console.WriteLine("{0}.distance={1}", it.M_CurrentItem.M_Label, MDistance(it.M_CurrentItem));
                if (MPredecessor(it.M_CurrentItem) != null)
                {
                    Console.WriteLine("{0}.predecessor={1}", it.M_CurrentItem.M_Label,
                        MPredecessor(it.M_CurrentItem).M_Label);
                }
            }

            Console.WriteLine("Shortest Paths {0}->*", m_source.M_Label);

            for (it.Begin(); !it.End(); it.Next())
            {

                foreach (var cGraphNode in m_shortestPaths[m_source][it.M_CurrentItem])
                {
                    Console.Write("{0},", cGraphNode.M_Label);
                }
                Console.WriteLine();
            }
        }

    }

}

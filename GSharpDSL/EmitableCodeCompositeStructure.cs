﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSharpDSL
{
    public abstract class EmitableCodeBlock
    {
        protected int m_tabIndentation;
        protected static int m_indenationSpaces = 4;

        internal int TabIndentation
        {
            get => m_tabIndentation;
            set => m_tabIndentation = value;
        }

        protected EmitableCodeBlock(int indentation)
        {
            m_tabIndentation = indentation;
        }

        public virtual void EnterScope() { m_tabIndentation++; AddNewLine(); }
        public virtual void LeaveScope() { m_tabIndentation--; AddNewLine(); }
        public abstract void AddCode(string text, bool ignoreIndentation = false);
        public abstract StringBuilder AssembleCode();
        public abstract void EmitToFile(StreamWriter outputFile);
        public abstract void AddNewLine(int n = 1);
        public abstract void AdjustIndentation(int baseIndentation);
    }

    public class ComboBlock : EmitableCodeBlock
    {
        protected List<EmitableCodeBlock> m_contents = new List<EmitableCodeBlock>();

        public ComboBlock(int indentation = 0) : base(indentation)
        {

        }

        public override void AddCode(string text, bool ignoreIndentation = false)
        {
            CodeBlock newBlock = ignoreIndentation == false ? new CodeBlock(text, m_tabIndentation) : new CodeBlock(text, -1);
            m_contents.Add(newBlock);
        }

        public void AddBlock(EmitableCodeBlock block)
        {
            block.AdjustIndentation(m_tabIndentation);
            m_contents.Add(block);
        }

        public override void AdjustIndentation(int baseIndentation)
        {
            m_tabIndentation += baseIndentation;
            foreach (EmitableCodeBlock ecb in m_contents)
            {
                ecb.AdjustIndentation(baseIndentation);
            }
        }

        public override void AddNewLine(int n = 1)
        {
            string newline = "";
            for (int i = 0; i < n; i++)
            {
                newline += "\n";
            }
            CodeBlock newBlock = new CodeBlock(newline);
            m_contents.Add(newBlock);
        }

        public override StringBuilder AssembleCode()
        {
            StringBuilder sb = new StringBuilder();
            foreach (EmitableCodeBlock ecb in m_contents)
            {
                sb.Append(ecb.AssembleCode());
            }

            return sb;
        }
        public override void EmitToFile(StreamWriter outputFile)
        {
            throw new NotImplementedException();
        }

    }

    public class CodeBlock : EmitableCodeBlock
    {
        private StringBuilder m_code;

        public CodeBlock(string code, int indentation = 0) : base(indentation)
        {
            m_code = new StringBuilder();
            m_code.Append(code);
        }

        public override void AddCode(string text, bool ignoreIndentation = false)
        {
            m_code.Append(text);
        }

        public override StringBuilder AssembleCode()
        {
            StringBuilder sb = new StringBuilder();
            if (m_tabIndentation < 0)
                sb.Append(m_code);
            else
                sb.Append(spacesString(m_tabIndentation * m_indenationSpaces) + m_code);
            return sb;
        }

        public override void EmitToFile(StreamWriter outputFile)
        {

        }

        public override void AddNewLine(int n = 1)
        {
            for (int i = 0; i < n; i++)
            {
                m_code.Append("\n");
            }
        }

        public override void AdjustIndentation(int baseIndentation)
        {
            if (m_tabIndentation >= 0)
                m_tabIndentation += baseIndentation;
        }

        private String spacesString(int spaces)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < spaces; i++)
            {
                sb.Append(" ");
            }

            return sb.ToString();
        }
    }

    public class GenericStructure : ComboBlock
    {
        public GenericStructure(int indentation) : base(indentation) { }

        public StringBuilder AssembleCodeContainer()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(AssembleCode());
            return sb;
        }

        public override void EmitToFile(StreamWriter outputFile)
        {
            StringBuilder sb = AssembleCodeContainer();
            outputFile.WriteLine(sb.ToString());
        }
    }
}

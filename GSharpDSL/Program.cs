﻿using System;
using System.Collections.Generic;
using GraphLibrary;
using GraphLibrary.Algorithms;

namespace GSharpDSL
{
    class Program
    {
        static void Main(string[] args)
        {
            //DSL_testFord();
            DSL_MyAlg1();
            DSL_MyAlg2();

            //------ RUN BOTH ALGORITHMS -------
            /*AlgOne a1 = new AlgOne(null,null);
            AlgTwo a2 = new AlgTwo(null, null);

            a1.Run();
            a2.Run();

            Console.WriteLine("Finished... Bank output is " + IOBank.GetItemFromBank("myoutputBank"));*/

            //test();
        }

        static void DSL_MyAlg1()
        {
            AlgorithmInterface algorithmInterface = new AlgorithmInterface();

            algorithmInterface
                .InterfaceName("AlgOne")
                .AddNamespaceImport("GraphLibrary")
                .AddNamespaceImport("System")
                .AddNamespaceImport("System.Collections.Generic")
                .AddNamespaceImport("GraphLibrary.Algorithms")
                .NodeInfo(typeof(NodePathInfo))
                    .CreateGetter("GetNodeInfo")
                    .CreateSetter("SetNodeInfo")
                    .Members()
                        .Member(typeof(int?)).MemberName("MDistance").end()
                        .Member(typeof(CGraphNode)).MemberName("MPredecessor").end()
                .end()
                .EdgeInfo(typeof(String))
                    .CreateSetter("SetEdgeInfo")
                    .CreateGetter("GetEdgeInfo")
                .end()
                .GraphInfo(typeof(Dictionary<CGraphNode, Dictionary<CGraphNode, Path>>))
                    .CreateGetter("GetGraphInfo")
                    .CreateSetter("SetGraphInfo")
                .end()
                .AddOutput()
                    .Name("myint")
                    .ConnectTo("myintBank")
                .end()
            .end();
        }

        static void DSL_MyAlg2()
        {
            AlgorithmInterface algorithmInterface = new AlgorithmInterface();

            algorithmInterface
                .InterfaceName("AlgTwo")
                .AddNamespaceImport("GraphLibrary")
                .AddNamespaceImport("System")
                .AddNamespaceImport("System.Collections.Generic")
                .AddNamespaceImport("GraphLibrary.Algorithms")
                .NodeInfo(typeof(int?))
                    .CreateGetter("GetNodeInfo")
                    .CreateSetter("SetNodeInfo")
                .end()
                .EdgeInfo(typeof(String))
                    .CreateSetter("SetEdgeInfo")
                    .CreateGetter("GetEdgeInfo")
                .end()
                .GraphInfo(typeof(Dictionary<CGraphNode, Dictionary<CGraphNode, Path>>))
                    .CreateGetter("GetGraphInfo")
                    .CreateSetter("SetGraphInfo")
                .end()
                .AddInput()
                    .Name("myinput")
                    .Type(typeof(int))
                    .ConnectionFrom("myintBank")
                .end()
                .AddOutput()
                    .Name("myoutput")
                    .ConnectTo("myoutputBank")
                .end()
            .end();
        }

        /*static void DSL_testFord()
        {
            AlgorithmInterface algorithmIoInterface = new AlgorithmInterface();

            algorithmIoInterface
                .InterfaceName("TestFord")
                .NodeInfo(typeof(NodePathInfo))
                    .Members()
                        .Member(typeof(int?)).MemberName("MDistance").end()
                        .Member(typeof(CGraphNode)).MemberName("MPredecessor").end()
                    .end()
                .EdgeInfo(typeof(int?)).end()
                .GraphInfo(typeof(Dictionary<CGraphNode, Dictionary<CGraphNode, Path>>))
                    .CreateGetter("ShortestPaths")
                .end()
            .end();
        }

        static void test()
        {
            CGraph mgraph = CGraph.CreateGraph();

            CGraphNode x1 = mgraph.CreateGraphNode<CGraphNode>("1");
            CGraphNode x2 = mgraph.CreateGraphNode<CGraphNode>("2");
            CGraphNode x3 = mgraph.CreateGraphNode<CGraphNode>("3");
            CGraphNode x4 = mgraph.CreateGraphNode<CGraphNode>("4");
            CGraphNode x5 = mgraph.CreateGraphNode<CGraphNode>("5");
            CGraphNode x6 = mgraph.CreateGraphNode<CGraphNode>("6");
            CGraphNode x7 = mgraph.CreateGraphNode<CGraphNode>("7");
            CGraphNode x8 = mgraph.CreateGraphNode<CGraphNode>("8");
            CGraphNode x9 = mgraph.CreateGraphNode<CGraphNode>("9");

            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x1, x2, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x2, x1, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x1, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x1, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x1, x4, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x4, x1, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x2, x4, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x4, x2, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x2, x3, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x3, x2, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x2, x6, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x6, x2, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x2, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x2, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x3, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x3, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x3, x6, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x6, x3, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x4, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x4, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x4, x7, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x7, x4, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x4, x8, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x8, x4, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x6, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x6, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x7, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x7, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x8, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x8, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x5, x9, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x9, x5, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x6, x8, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x8, x6, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x6, x9, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x9, x6, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x7, x8, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x8, x7, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x8, x9, GraphType.GT_DIRECTED);
            mgraph.AddGraphEdge<CGraphEdge, CGraphNode>(x9, x8, GraphType.GT_DIRECTED);


            // 2. Associate weights with the edges of the graph
            CGraphQueryInfo<int, int, int> weights = new CGraphQueryInfo<int, int, int>(mgraph, TEST_FORD_KEY);

            weights.CreateInfo(x1, x2, 1);
            weights.CreateInfo(x2, x1, 1);
            weights.CreateInfo(x1, x5, 1);
            weights.CreateInfo(x5, x1, 1);
            weights.CreateInfo(x1, x4, 1);
            weights.CreateInfo(x4, x1, 1);
            weights.CreateInfo(x2, x4, 3);
            weights.CreateInfo(x4, x2, 3);
            weights.CreateInfo(x2, x3, 1);
            weights.CreateInfo(x3, x2, 1);
            weights.CreateInfo(x2, x6, 3);
            weights.CreateInfo(x6, x2, 3);
            weights.CreateInfo(x2, x5, 1);
            weights.CreateInfo(x5, x2, 1);
            weights.CreateInfo(x3, x5, 3);
            weights.CreateInfo(x5, x3, 3);
            weights.CreateInfo(x3, x6, 1);
            weights.CreateInfo(x6, x3, 1);
            weights.CreateInfo(x4, x5, 1);
            weights.CreateInfo(x5, x4, 1);
            weights.CreateInfo(x4, x7, 1);
            weights.CreateInfo(x7, x4, 1);
            weights.CreateInfo(x4, x8, 3);
            weights.CreateInfo(x8, x4, 3);
            weights.CreateInfo(x5, x6, 1);
            weights.CreateInfo(x6, x5, 1);
            weights.CreateInfo(x5, x7, 3);
            weights.CreateInfo(x7, x5, 3);
            weights.CreateInfo(x5, x8, 1);
            weights.CreateInfo(x8, x5, 1);
            weights.CreateInfo(x5, x9, 3);
            weights.CreateInfo(x9, x5, 3);
            weights.CreateInfo(x6, x8, 3);
            weights.CreateInfo(x8, x6, 3);
            weights.CreateInfo(x6, x9, 1);
            weights.CreateInfo(x9, x6, 1);
            weights.CreateInfo(x7, x8, 1);
            weights.CreateInfo(x8, x7, 1);
            weights.CreateInfo(x8, x9, 1);
            weights.CreateInfo(x9, x8, 1);

            // 3. Run the TestFord algorithm
            TestFord tf = new TestFord(mgraph, x1, TEST_FORD_KEY);

            tf.FindAllPairsShortestPaths();

            // 4. Print Paths
            TestFordQueryInfo shortestPath = new TestFordQueryInfo(mgraph, tf);
            CIt_GraphNodes i = new CIt_GraphNodes(mgraph);
            CIt_GraphNodes j = new CIt_GraphNodes(mgraph);
            Dictionary<CGraphNode, Dictionary<CGraphNode, Path>> paths =
                shortestPath.ShortestPaths();
            for (i.Begin(); !i.End(); i.Next())
            {
                Console.WriteLine();
                for (j.Begin(); !j.End(); j.Next())
                {
                    Console.WriteLine();
                    if (i.M_CurrentItem != j.M_CurrentItem)
                    {
                        Console.Write(paths[i.M_CurrentItem][j.M_CurrentItem]);
                    }
                }
            }
        }*/
    }
}

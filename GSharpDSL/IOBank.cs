﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSharpDSL
{
    /*
     * IOBank :
     *      -> First Object ~> Key for entry
     *      -> Second Object ~> value for given key, could either be some raw data or a key that points to data stored on graph.
     */
    public static class IOBank
    {
        private static Dictionary<string,object> Bank = new Dictionary<string, object>();

        public static void AddItemToBank(string itemKey, object itemValue)
        {
            if (Bank.ContainsKey(itemKey)) throw new Exception("Given Key name : \"" + itemKey + "\" already exists in IOBank");
            Bank.Add(itemKey, itemValue);
        }

        public static object GetItemFromBank(string itemKey)
        {
            if (!Bank.ContainsKey(itemKey)) throw new Exception("Given Key name : \"" + itemKey + "\" does not exist in IOBank");
            return Bank[itemKey];
        }

        public static void SetValueForItemInBank(string itemKey, object itemValue)
        {
            if (!Bank.ContainsKey(itemKey)) throw new Exception("Given Key name : \"" + itemKey + "\" does not exist in IOBank");
            Bank[itemKey] = itemValue;
        }

        public static void RemoveItemFromBank(string itemKey)
        {
            if (!Bank.ContainsKey(itemKey)) throw new Exception("Given Key name : \"" + itemKey + "\" does not exist in IOBank");
            Bank.Remove(itemKey);
        }

        public static void ClearBank()
        {
            Bank.Clear();
        }

        public static Dictionary<string, object> GetBankObject()
        {
            return Bank;
        }
    }
}

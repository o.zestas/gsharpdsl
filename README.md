## GSharp Interal DSL

GSharp is an internal DSL developed for easier representation of graphs and graph algorithms (written in C#).\
Developed alongside my supervisor, [Grigoris Dimitroulakos](http://users.uop.gr/~dhmhgre/)
